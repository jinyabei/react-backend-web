import { Navigate, createBrowserRouter } from "react-router-dom";
import Main from "../pages/main";
import Home from "../pages/home";
import Mall from '../pages/mall';
import User from "../pages/user";
import PageOne from "../pages/other/pageOne";
import PageTwo from "../pages/other/pageTwo";
import Login from "../pages/login";

//注意routes应该是一个数组
const routes = [
  {
    path:"/",
    //注意Component要大写
    Component:Main,
    //children要小写
    children:[
      //重定向
      {
        path:"/",
        element:<Navigate to="home" replace />
      },
      {
        path:'home',
        Component:Home
      },
      {
        path:'mall',
        Component:Mall
      },
      {
        path:'user',
        Component:User
      },
      {
        path:'other',
        children:[
          {
            path:'pageOne',
            Component:PageOne
          },
          {
            path:'pageTwo',
            Component:PageTwo
          }
        ]
      }
    ]
  },
  {
    path:'/login',
    Component:Login
  }
]

export default createBrowserRouter(routes)