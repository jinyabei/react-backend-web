import React, { useEffect,useState } from "react";
import { Button,Form,Input,Table,Popconfirm,Modal, InputNumber, Select, DatePicker } from "antd";
import './user.css'
import { getUser,addUser, editUser, delUser } from "../../api";
import dayjs from "dayjs";

const User = ()=>{
  const [listData,setListData] = useState({
    name:''
  })
  const [tableData,setTableData] = useState([])
  //0-新增 1-编辑
  const [modalType,setModalType] = useState(0)

  const [isModalOpen,setIsModelOpen] = useState(false)

  const [form] = Form.useForm()

  const columns = [
    {
      title: '姓名',
      dataIndex: 'name',
    },
    {
      title: '年龄',
      dataIndex: 'age',
    },
    {
      title: '性别',
      dataIndex:'sex',
      render:(val)=>{
        return val?'女':'男'
      }
    },
    {
      title:'出生日期',
      dataIndex:"birth",
    },
    {
      title:'地址',
      dataIndex:"addr",
    },
    {
      title:'操作',
      render:(row)=>{
        return (
          <div className="flex-box" style={{'width':'100px'}}>
            <Button style={{marginRight:'5px'}} onClick={()=>handleClick('edit',row)}>修改</Button>
            <Popconfirm
              title="提示"
              description="此操作讲删除该用户，是否继续?"
              okText="确认"
              cancelText="取消"
              onConfirm = {()=>handleDelete(row)}
            >
              <Button danger>删除</Button>
            </Popconfirm>
          </div>
        )
      }
    }
  ];
  //新增
  const handleClick = (type,row)=>{
    setIsModelOpen(!isModalOpen)
    if(type==='add'){
      setModalType(0)
    }else if(type==='edit'){
      const cloneDate = JSON.parse(JSON.stringify(row))
      cloneDate.birth = dayjs(cloneDate.birth)
      form.setFieldsValue(cloneDate)
      setModalType(1)
    }
  }

  //点击弹窗确定
  const handleOk = ()=>{
    form.validateFields().then((val)=>{
      val.birth = dayjs(val.birth).format("YYYY-MM-DD")
      if(modalType){
        editUser(val).then(()=>{
          handleCancel();
          getTableData();
        })
      }else{
        addUser(val).then(()=>{
          handleCancel();
          getTableData();
        })
      }
    }).catch((err)=>{
      console.log(err)
    })
  }

  //关闭
  const handleCancel = ()=>{
    setIsModelOpen(!isModalOpen)
    form.resetFields()
  }
  
  //提交
  const handleFinish = (e)=>{
    setListData({
      name: e.keyword
    });
  }

  const handleDelete = (row)=>{
    delUser(row).then(()=>{
      getTableData()
    })
  }

  const getTableData = ()=>{
    getUser(listData).then(({data})=>{
      setTableData(data.list)
    })
  }
  useEffect(()=>{
    getTableData()
  },[listData])
  return(
    <div className="user">
      <div className="flex-box">
        <Button type="primary" onClick={()=>handleClick('add')}>+新增</Button>
        <Form layout="inline" onFinish={handleFinish}>
          <Form.Item name="keyword">
            <Input placeholder="请输入用户名"/>
          </Form.Item>
          <Form.Item>
           <Button htmlType="submit" type="primary">搜索</Button>
          </Form.Item>
        </Form>
      </div>
      <Table style={{marginTop:'10px'}} dataSource={tableData} columns={columns} rowKey={'id'}/>
      <Modal title={modalType?'编辑用户':"新增用户"} open={isModalOpen} onOk={handleOk} onCancel={handleCancel}
       okText="确定"
       cancelText="取消"
      >
        <Form 
          form={form}
          labelCol={{
            span:6
          }}
          wrapperCol={{
            span:18
          }}
          labelAlign="left">
            { modalType&&<Form.Item 
              name="id"  
              hidden>
              <Input/>
            </Form.Item>}
            <Form.Item 
              name="name"  
              label="姓名"
              rules={[{ required: true, message: '请输入姓名' }]}>
              <Input placeholder="请输入姓名" />
            </Form.Item>
            <Form.Item 
              name="age"  
              label="年龄"
              rules={[
                { required: true, message: '请输入年龄' },
                {type:'number',message:"年龄必须是数字"}
                ]}>
              <InputNumber placeholder="请输入年龄" />
            </Form.Item>
            <Form.Item 
              name="sex"  
              label="性别"
              rules={[{ required: true, message: '请选择性别' }]}>
              <Select options={[
                  {value:0,label:'男'},
                  {value:1,label:'女'}
                ]}
                placeholder="请选择性别"
                />
            </Form.Item>
            <Form.Item 
              name="birth"  
              label="出生日期"
              rules={[{ required: true, message: '请选择出生日期' }]}>
              <DatePicker placeholder="请选择出生日期" format="YYYY/MM/DD"/>
            </Form.Item>
            <Form.Item 
              name="addr"  
              label="地址"
              rules={[{ required: true, message: '请填写地址' }]}>
              <Input placeholder="请填写地址" />
            </Form.Item>
          </Form>
      </Modal>
    </div>
  )
}

export default User