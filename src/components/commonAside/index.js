import { createElement } from "react";
import { Menu,Layout } from 'antd';
import * as Icon from "@ant-design/icons"
import MenuCofig from "../../config/index.js"
import { useNavigate } from "react-router-dom";
import {useDispatch} from 'react-redux'
import {selectMenuList} from '../../store/reducers/tab.js'

const { Sider } = Layout;

//动态获取icon函数
const iconToElement = (name)=>{
  return createElement(Icon[name])
}

//处理菜单数据
const items = MenuCofig.map((icon)=>{
  //没有子菜单
  const child = {
    key:icon.path,
    icon:iconToElement(icon.icon),
    label:icon.label,
  }

  //有子菜单
  if(icon.children){
    child.children = icon.children.map((item)=>{
      return {
        key:item.path,
        icon:iconToElement(item.icon),
        label:item.label,
      }
    })
  }
  return child
})

const CommonAside = ({collapse})=>{
  const navigate = useNavigate()
  const dispatch = useDispatch()

  const setTabsList = (val)=>{
    dispatch(selectMenuList(val))
  }

  //点击菜单
const selectMenu = (e)=>{
  let data
  MenuCofig.forEach(item=>{
    if(item.path===e.keyPath[e.keyPath.length-1]){
      data = item
      //有子菜单
      if(e.keyPath.length>1){
        data = item.children.find(child=>{
          return child.path === e.key
        })
      }
    }
  })
  setTabsList({
    path:data.path,
    name:data.name,
    label:data.label,
  })
  navigate(e.key)
}

  return(
    <Sider trigger={null} collapsible collapsed={collapse}>
      <h3 className="app-name">{collapse?'后台':'通用后台管理系统'}</h3>
      <Menu
        theme="dark"
        mode="inline"
        defaultSelectedKeys={['1']}
        style = {{
          height:'100%'
        }}
        items={items}
        onClick={selectMenu}
      />
    </Sider>
  )
}

export default CommonAside