import { createSlice } from "@reduxjs/toolkit";

const tabSlice = createSlice({
  name:'tab',
  initialState:{
    isCollapse:false,
    tabList:[{
      path:'/',
      name:'home',
      label:"首页"
    }],
    currentMenu:{
      path:'/',
      name:'home',
      label:"首页"
    },
  },
  reducers:{
    collapseMenu:state=>{
      state.isCollapse = !state.isCollapse
    },
    selectMenuList:(state,{payload:val})=>{
      if(val.name!=='home'){
        state.currentMenu = val;
        const result = state.tabList.findIndex(item=>item.name===val.name)
        if(result===-1){
          state.tabList.push(val)
        }
      }else{
        state.currentMenu = {
          path:'/',
          name:'home',
          label:"首页"
        }
      }
    },
    closeTab:(state,{payload:val})=>{
      let res = state.tabList.findIndex(item=>item.name===val.name)
      state.tabList.splice(res,1)
    },
    setCurrentMenu:(state,{payload:val})=>{
      if(val.name ==='home'){
        state.currentMenu = {
          path:'/',
          name:'home',
          label:"首页"
        }
      }else{
        state.currentMenu = val
      }
    },
  },
})

export const {collapseMenu,selectMenuList,closeTab,setCurrentMenu} = tabSlice.actions
export default tabSlice.reducer