import { Row,Col,Card, Table } from "antd";
import React,{useEffect,useState,createElement} from "react";
import * as Icon from "@ant-design/icons"
import './index.css'
import { getData } from "../../api";
import MyEcharts from "../../components/Echarts"

//table列的数据
const columns = [
  {
    title:'课程',
    dataIndex:'name'
  },
  {
    title:'今日购买',
    dataIndex:'todayBuy'
  },
  {
    title:'本月购买',
    dataIndex:'monthBuy'
  },
  {
    title:'总购买',
    dataIndex:'totalBuy'
  }
]

//订单统计数据
const countData = [
  {
    "name":"今日支付订单",
    "value":1234,
    "icon":"CheckCircleOutlined",
    "color":"#2ec7c9"
  },
  {
    "name":"今日收藏订单",
    "value":3421,
    "icon":"ClockCircleOutlined",
    "color":"#ffb980"
  },
  {
    "name":"今日未支付订单",
    "value":1234,
    "icon":"CloseCircleOutlined",
    "color":"#5ab1ef"
  },
  {
    "name":"本月支付订单",
    "value":1234,
    "icon":"CheckCircleOutlined",
    "color":"#2ec7c9"
  },
  {
    "name":"本月收藏订单",
    "value":3421,
    "icon":"ClockCircleOutlined",
    "color":"#ffb980"
  },
  {
    "name":"本月未支付订单",
    "value":1234,
    "icon":"CloseCircleOutlined",
    "color":"#5ab1ef"
  }
]

//动态获取icon函数
const iconToElement = (name)=>{
  return createElement(Icon[name])
}

const Home = ()=>{
  const [tableData,setTableData] = useState([])

  const  [echartData,setEchartData] = useState([])
  //首次加载(首次渲染完成)
  useEffect(()=>{
    getData().then(({data:{data}})=>{
      const {tableData,orderData,userData,videoData} = data
      setTableData(tableData)
      const order = orderData
      const xData = order.date
      const keyArr = Object.keys(order.data[0])
      let series = []
      keyArr.forEach(key=>{
        series.push({
          name:key,
          data:order.data.map((item)=>item[key]),
          type:'line'
        })
      })
      setEchartData({
        order:{
          xData,
          series
        },
        user:{
          xData:userData.map(item=>item.date),
          series:[
            {
              name:'新增用户',
              data:userData.map(item=>item.new),
              type:'bar'
            },
            {
              name:'活跃用户',
              data:userData.map(item=>item.active),
              type:'bar'
            }
          ]
        },
        video:{
          series:[{
            data:videoData,
            type:'pie'
          }]
        }
      })
    })
  },[]);
  return(
    <Row className="home">
      <Col span={8}>
        <Card
        hoverable
        >
          <div className="user">
            <img src="https://os.alipayobjects.com/rmsportal/QBnOOoLaAfKPirc.png" alt=""/>
            <div className="userinfo">
              <p className="name">Admin</p>
              <p className="access">超级管理员</p>
            </div>
          </div>
          <div className="login-info">
            <p>
            上次登陆时间:
              <span>2021-07-19</span>
            </p>
            <p>
              上次登陆地点:
              <span>武汉</span>
            </p>
          </div>
        </Card>
        <Card>
          <Table columns={columns} rowKey={"name"} dataSource={tableData} pagination={false}/>
        </Card>  
      </Col>
      <Col span={16}>
        <div className="num">
          {
            countData.map((item,index)=>{
              return(
                <Card key={index}>
                  <div className="icon-box" style={{background:item.color}}>
                   {iconToElement(item.icon)}
                  </div>
                  <div className="detail">
                    <p className="num">￥{item.value}</p>
                    <p className="txt">{item.name}</p>
                  </div>
                </Card>
              )
            })
          }
        </div>
          {echartData.order&&<MyEcharts chartData={echartData.order} style={{height:'280px'}}/>}
        <div className="graph">
          {echartData.user&&<MyEcharts chartData={echartData.user} style={{height:'240px',width:'50%'}}/>}
          {echartData.video&&<MyEcharts chartData={echartData.video} isAxisChart={false} style={{height:'260px',width:'50%'}}/>}
        </div>
      </Col>
    </Row>
  )
}

export default Home