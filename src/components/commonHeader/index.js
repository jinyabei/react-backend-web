import {
  MenuUnfoldOutlined,
  MenuFoldOutlined} from '@ant-design/icons';
import { Layout,theme, Button,Avatar,Dropdown } from 'antd';
import  {useDispatch} from "react-redux"
import { collapseMenu } from '../../store/reducers/tab';
import { useNavigate } from 'react-router-dom';

const { Header} = Layout;

const CommonHeader = ({collapse})=>{
  const navigate = useNavigate()

  const logout = ()=>{
    localStorage.removeItem("token")
    navigate("/login")
  }
  const items = [
    {
      key: '1',
      label: (
        <a target="_blank" rel="noopener noreferrer">
         个人中心
        </a>
      ),
    },
    {
      key: '2',
      label: (
        <a onClick={()=>logout()}target="_blank" rel="noopener noreferrer">
          退出
        </a>
      ),
    }
  ];

  const dispatch = useDispatch()

  const setCollapsed = ()=>{
    dispatch(collapseMenu())
  }
  
  const {} = theme.useToken();
  return (
    <Header
      style={{
        padding: 0,
        display:"flex",
        justifyContent:'space-between',
        alignItems:'center'
      }}
    >
      <Button
        type="text"
        icon={collapse ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
        onClick={() => setCollapsed()}
        style={{
          fontSize: '16px',
          width: 64,
          height: 32,
          backgroundColor:'#fff',
        }}
      />
      <Dropdown
        menu={{
          items
        }}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Avatar size={36} src="https://zos.alipayobjects.com/rmsportal/ODTLcjxAfvqbxHnVXCYX.png"/>
        </a>
      </Dropdown>
    </Header>
  )
}

export default CommonHeader