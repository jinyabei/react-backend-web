import { Space, Tag } from "antd"
import './index.css'
import { useSelector } from "react-redux"
import {closeTab,setCurrentMenu} from '../../store/reducers/tab'
import { useDispatch} from "react-redux"
import { useLocation,useNavigate } from "react-router-dom"

const CommonTag = ()=>{
  const tabList = useSelector(store=>store.tab.tabList)
  const currentMenu = useSelector(store=>store.tab.currentMenu)
  const dispatch = useDispatch()
  const action = useLocation()
  const navigate = useNavigate()

  const handleClose = (tag,index)=>{
    let length = tabList.length-1
    dispatch(closeTab(tag))
    //关闭的不是当前标签
    if(tag.path !==action.pathname){
      return;
    }
    if(index===length){
      const curData =  tabList[index-1]
      dispatch(setCurrentMenu(curData))
      navigate(curData.path);
    }else{
      //如果tags至少存在一个数据，则选择后一个tags
      if(tabList.length>1){
        const nextData =  tabList[index+1]
        dispatch(setCurrentMenu(nextData))
        navigate(nextData.path);
      }
    }
  }

  const handleChange = (tag)=>{
    dispatch(setCurrentMenu(tag))
    navigate(tag.path);
  }
  //tab的显示
  const setTag = (flag,item,index)=>{
    return (
      flag?
      <Tag key={item.name} color="#2db7f5" closeIcon onClose={()=>handleClose(item,index)}>
        {item.label}
      </Tag>:
      <Tag onClick={()=>handleChange(item)} key={item.name}>{item.label}</Tag>
    )
  }
  return(
    <Space className="common-tag" sizs={[0,8]} wrap>
      {
        currentMenu&&currentMenu.name&&tabList.map((item,index)=>setTag(item.name===currentMenu.name,item,index))
      }
    </Space>
  )
}

export default CommonTag